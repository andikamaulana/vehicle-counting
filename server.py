import socket
from _thread import *
import threading
import numpy as np
import json
import pickle
import struct
import cv2
import Deteksi as DE
# from ocr_plate import Plate

# plate = Plate()

print_lock = threading.Lock()
id_f=0;

def threaded(c):
	global id_f
	data = b""
	payload_size = struct.calcsize(">L")
	print("payload_size: {}".format(payload_size))
	if id_f>3:
		id_f=0
	while True:
		while len(data) < payload_size:
			data += c.recv(4096)

		print("Done Recv: {}".format(len(data)))
		packed_msg_size = data[:payload_size]
		data = data[payload_size:]
		msg_size = struct.unpack(">L", packed_msg_size)[0]
		print("msg_size: {}".format(msg_size))
		while len(data) < msg_size:
			data += c.recv(4096)
		frame_data = data[:msg_size]
		data = data[msg_size:]

		frame=pickle.loads(frame_data, fix_imports=True, encoding="bytes")
		fileName = "frame/"+str(id_f)+".jpg"
		cv2.imwrite(fileName,frame)
		platt= DE.detector(fileName)
		print("plat : "+platt)
		id_f+=1
		print("done")
		print_lock.release()
		break
	print("close")
	c.close()


def Main():
	host = ""
	port = int(input("port :"))
	s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
	s.bind((host,port))
	s.listen(5)
	while True:
		c,addr = s.accept()
		print_lock.acquire()
		print('Connected to :',addr[0],':',addr[1])
		start_new_thread(threaded,(c,))
	s.close()


if __name__ == '__main__':
	Main()
