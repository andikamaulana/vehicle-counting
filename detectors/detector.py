def get_bounding_boxes(frame, model):
    from detectors.yolo import get_bounding_boxes as gbb
    return gbb(frame)