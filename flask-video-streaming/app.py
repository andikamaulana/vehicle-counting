#!/usr/bin/env python
from importlib import import_module
import os
from flask import Flask, render_template, Response, request
import cv2
import numpy as np
# import camera driver
# if os.environ.get('CAMERA'):
#     Camera = import_module('camera_' + os.environ['CAMERA']).Camera
# else:
#     from camera import Camera
Camera = import_module('camera_opencv').Camera

# Raspberry Pi camera module (requires picamera package)
# from camera_pi import Camera

app = Flask(__name__)


@app.route('/')
def index():
    """Video streaming home page."""
    return render_template('index.html')


def gen(camera,no):
    """Video streaming generator function."""
    while True:
        # frame = camera.get_frame(no)

        frame = cv2.imread("../frame/"+str(no)+".jpg")
        frames = cv2.imencode('.jpg', frame)[1].tobytes()
        if no==4:
            frames=camera.get_frame()
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frames + b'\r\n')


@app.route('/video_feed',methods = ['GET'])
def video_feed():
    fileno = request.args.get('id', None)
    """Video streaming route. Put this in the src attribute of an img tag."""
    return Response(gen(Camera(),fileno),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=1235, threaded=True)
