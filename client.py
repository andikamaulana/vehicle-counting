import socket
import json
import pickle
import cv2
import struct

host = '127.0.0.1'
port = int(input("port :"))
def send(crop):
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect((host,port))

    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
    result, frame = cv2.imencode('.jpg', crop, encode_param)
    crop = pickle.dumps(crop,0)
    size = len(crop)
    s.sendall(struct.pack(">L", size) + crop)