import time
import math
import argparse
import mxnet as mx
from gluoncv import model_zoo, data
from dataset import load_image, visualize
from utils import color_normalize, plate_labels, reconstruct_plates, Vocabulary
from wpod_net import WpodNet
from ocr_net import OcrNet
import cv2


dims=208
threshold = 0.9
plt_w = 144
plt_h = 48
seq_len = 8
no_yolo = True
context = mx.cpu(0)
beam = True
beam_size = 5
plt_hw = (plt_h, plt_w)
print("Loading model...")
wpod = WpodNet()
wpod.load_parameters("model/wpod_net.params", ctx=context)
vocab = Vocabulary()
vocab.load("model/vocabulary.json")
ocr = OcrNet(plt_hw, vocab.size(), seq_len)
ocr.load_parameters("model/ocr_net.params", ctx=context)

def fixed_crop(raw, bbox):
    x0 = max(int(bbox[0].asscalar()), 0)
    x0 = min(int(x0), raw.shape[1])
    y0 = max(int(bbox[1].asscalar()), 0)
    y0 = min(int(y0), raw.shape[0])
    x1 = max(int(bbox[2].asscalar()), 0)
    x1 = min(int(x1), raw.shape[1])
    y1 = max(int(bbox[3].asscalar()), 0)
    y1 = min(int(y1), raw.shape[0])
    return mx.image.fixed_crop(raw, x0, y0, x1 - x0, y1 - y0)

#plate : array plate, context : cpu(0)
def recognize_plate(vocab, ocr, plate):
    global beam
    global beam_size
    global context

    ts = time.time()
    x = color_normalize(plate).transpose((2, 0, 1)).expand_dims(0)
    enc_y, self_attn = ocr.encode(x.as_in_context(context))
    sequence = [vocab.char2idx("<GO>"),vocab.char2idx(80)]
    platt =""
    while True:
        #list idx char in vocab
        tgt = mx.nd.array(sequence, ctx=context).reshape((1, -1))
        #len char
        tgt_len = mx.nd.array([len(sequence)], ctx=context) 
        #enc_y = frame per char
        y, context_attn = ocr.decode(tgt, tgt_len, enc_y)
    
        index = mx.nd.argmax(y, axis=2)
        # print(f"idx : {index}")
        #convert idx vocab to string
        char_token = index[0, -1].asscalar()
        # print(f"",end="")
        sequence += [char_token]
        if char_token == vocab.char2idx("<EOS>"):
            break;
        platt+= vocab.idx2char(char_token)
    # print("")
    return platt

def detect_plate(wpod, vocab, ocr, raw):
    global dims
    global threshold
    global plt_hw
    global beam
    global beam_size
    global context

    h = raw.shape[0]
    w = raw.shape[1]
    f = min(288 * max(h, w) / min(h, w), 608) / min(h, w)
    ts = time.time()
    img = mx.image.imresize(
        raw,
        int(w * f) + (0 if w % 16 == 0 else 16 - w % 16),
        int(h * f) + (0 if h % 16 == 0 else 16 - h % 16)
    )
    x = color_normalize(img).transpose((2, 0, 1)).expand_dims(0)
    y = wpod(x.as_in_context(context))
    probs = y[0, :, :, 0]
    affines = y[0, :, :, 2:]
    labels = plate_labels(img, probs, affines, dims, 16, threshold)

    plates = reconstruct_plates(raw, [pts for pts, _ in labels], (plt_hw[1], plt_hw[0]))
    print("wpod profiling: %f" % (time.time() - ts))
    if len(plates)>0:
        cv2.imwrite("plattt.jpg",plates[0].asnumpy())
        platt = recognize_plate(vocab, ocr, plates[0])
    else:
        platt=""
    return platt

def detector(images):
    raw = load_image(images)
    platt = detect_plate(wpod, vocab, ocr, raw)
    return platt
